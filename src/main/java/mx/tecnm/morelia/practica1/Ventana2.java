/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.tecnm.morelia.practica1;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author link
 */
public class Ventana2 extends JFrame {
    
    Ventana2() {
        super("Ventana 2");
        
        this.setSize(300,200);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        
        JButton boton = new JButton("Ok");
        boton.addActionListener((ActionEvent e) -> { cerrar(); });
        add(boton);
    }
    
    Ventana2(String nombre) {
        super("Ventana 2");
        
        this.setSize(300,200);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLocationRelativeTo(null);
        
        Container c = getContentPane();
        c.setLayout(new FlowLayout());
        
        JLabel label = new JLabel("Hola " + nombre);
        label.setForeground(Color.blue);
        c.add(label);
        
        JButton boton = new JButton("Ok");
        boton.addActionListener((ActionEvent e) -> { cerrar(); });
        c.add(boton);
    }
    
    private void cerrar() {
        this.dispose();
    }
    
}
